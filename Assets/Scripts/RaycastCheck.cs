﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastCheck : MonoBehaviour
{
    [Tooltip("The maximum distance for noticing points.")]
    public float MaxDistance = 30000000f;
    [Tooltip("The dialog used for displaying the point info.")]
    public PointDetailsDialog PointDetailsDialog;
    private Transform _cameraTransform;

    void Start()
    {
        _cameraTransform = Camera.main.transform;
    }

    void FixedUpdate()
    {
        RaycastHit raycastHit;

        if (Physics.Raycast(_cameraTransform.position, _cameraTransform.transform.forward, out raycastHit, MaxDistance))
        {
            GameObject pointOfInterest = raycastHit.collider.gameObject;
            PointOfInterest point = pointOfInterest.GetComponent<PointOfInterest>();

            PointDetailsDialog.Show(point);
        }
        else
        {
            PointDetailsDialog.Hide();
        }
    }
}
