using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class PointOfInterestManager : MonoBehaviour
{
    [Tooltip("The object which will contain all points of interest.")]
    public GameObject PointContainer;
    [Tooltip("The point of interest prefab.")]
    public GameObject PointOfInterestPrefab;
    [Tooltip("The script containing the last GPS position of the camera.")]
    public CameraGPSPosition CameraGPSPosition;
    public float MaxDistance = 5000000f;
    public float NumberOfPoints = 5f;
    private List<PointOfInterest> _pointsOfInterest = new List<PointOfInterest>();

    void Start()
    {
        //UpdatePoints();
    }

    public void UpdatePoints()
    {
        StartCoroutine(GetNewPoints());
    }

    private IEnumerator GetNewPoints()
    {
        string url = APIHelper.GetPointsByDistanceQuery(CameraGPSPosition.CurrentPosition, MaxDistance, NumberOfPoints);
        Debug.Log(url);

        UnityWebRequest request = UnityWebRequest.Get(url);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            yield break;
        }

        string response = string.Format("{{\"points\":{0}}}", request.downloadHandler.text);
        Debug.Log(response);

        JsonPointOfInterestList pointList = JsonUtility.FromJson<JsonPointOfInterestList>(response);

        StartCoroutine(RefreshPoints(pointList));
    }

    private IEnumerator RefreshPoints(JsonPointOfInterestList pointList)
    {
        //Iterate over all existing points
        for (int i = _pointsOfInterest.Count - 1; i >= 0; --i)
        {
            //If the old point is in the new points, remove it from the new points, since it exists in the scene
            int pointsRemoved = pointList.points.RemoveAll(point => point.id == _pointsOfInterest[i].Id);
            
            //If the old point is not in the new points, remove the old point
            if (pointsRemoved == 0)
            {
                Destroy(_pointsOfInterest[i].gameObject);
                _pointsOfInterest.RemoveAt(i);
            }
            yield return null;
        }

        //Now all the new points which previously didn't exist are in the point list, iterate and instantiate
        for (int i = pointList.points.Count - 1; i >= 0; --i)
        {
            JsonPointOfInterest poi = pointList.points[i];
            GameObject pointOfInterest = Instantiate(PointOfInterestPrefab);

            SetPointTransformAndDimensions(poi, pointOfInterest);
            yield return null;
        }
    }

    private void SetPointTransformAndDimensions(JsonPointOfInterest poi, GameObject pointOfInterest)
    {
        //Position
        Vector2 cameraGPSPosition = CameraGPSPosition.CurrentPosition;
        Vector2 newPointPosition = new Vector2(poi.latitude, poi.longitude);
        Vector3 offset = GPSHelper.GPSPointsToWorldOffset(cameraGPSPosition, newPointPosition);
        pointOfInterest.transform.position = CameraGPSPosition.transform.position + offset;

        //Rotation
        pointOfInterest.transform.rotation = Quaternion.identity;

        //Parent
        pointOfInterest.transform.parent = PointContainer.transform;

        //Collider dimensions
        BoxCollider collider = pointOfInterest.GetComponent<BoxCollider>();
        collider.size = new Vector3(poi.width, poi.height, poi.length);
        collider.center = new Vector3(0, poi.height / 2, 0);

        //Item details
        PointOfInterest point = pointOfInterest.GetComponent<PointOfInterest>();
        point.SetDetails(poi);
        _pointsOfInterest.Add(point);
    }
}