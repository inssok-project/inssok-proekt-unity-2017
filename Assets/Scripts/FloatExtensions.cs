using System;

public static class FloatExtensions
{
    public static double ToRadians(this float angle)
    {
        return (angle * Math.PI) / 180;
    }

}