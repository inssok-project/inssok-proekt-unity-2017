using System.Text;
using UnityEngine;

public class APIHelper
{
    public static string ApiUrl = "http://192.168.178.31:8000/";

    public static string GetPointsByDistanceQuery(Vector2 origin, float maxDistance, float numberOfPoints)
    {
        StringBuilder stringBuilder = new StringBuilder();

        //Base URL
        stringBuilder.Append(ApiUrl);
        stringBuilder.Append("pois_by_distance?");

        //Query parameters
        stringBuilder.AppendFormat("lat={0}&", origin.x.ToString("F17"));
        stringBuilder.AppendFormat("lng={0}&", origin.y.ToString("F17"));
        stringBuilder.AppendFormat("maxDistance={0}&", maxDistance.ToString("F1"));
        stringBuilder.AppendFormat("numberOfPoints={0}/", numberOfPoints);

        return stringBuilder.ToString();
    }

    public static string GetImageByNameQuery(string imageName)
    {
        StringBuilder stringBuilder = new StringBuilder();

        //Base URL
        stringBuilder.Append(ApiUrl);
        stringBuilder.Append("storage/pointImages/");
        stringBuilder.Append(imageName);

        return stringBuilder.ToString();
    }
}