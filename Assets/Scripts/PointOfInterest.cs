using System;
using UnityEngine;

public class PointOfInterest : MonoBehaviour
{
    public int Id { get; set; }
    public Vector2 Position { get; set; }
    public Vector3 Dimensions { get; set; }
    public string ImageName { get; set; }
    public string PointName { get; set; }
    public string PointDescription { get; set; }

    public void SetDetails(JsonPointOfInterest poi)
    {
        Id = poi.id;
        Position = new Vector2(poi.latitude, poi.longitude);
        Dimensions = new Vector3(poi.width, poi.height, poi.length);
        ImageName = poi.image;
        PointName = poi.name;
        PointDescription = poi.description;
    }
}