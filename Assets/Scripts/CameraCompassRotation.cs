﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraCompassRotation : MonoBehaviour
{
    public float UpdateInterval;
    
    void Start()
    {
        Input.compass.enabled = true;
        Input.location.Start();

        StartCoroutine(UpdateCameraRotationWithCompass());

        // Vector3 origin = GPSHelper.GPSPointsToWorldPosition(new Vector2(40, 40));
        // Vector3 destination = GPSHelper.GPSPointsToWorldPosition(new Vector2(50, 50));
        // float distance = GPSHelper.Haversine(new Vector2(40, 40), new Vector2(50, 50));
        // float localDistance = Vector3.Distance(origin, destination);
        // Vector3 originToDestination = GPSHelper.GPSPointsToWorldOffset(new Vector2(40, 40), new Vector2(50, 50));
        // Debug.LogFormat("Origin world position: {0}\nDestination world position: {1}\nDistance by haversine: {2}\nDistance by unity: {3}\nDistance vector by unity: {4}",
        // origin, destination, distance, localDistance, originToDestination);
    }

    private IEnumerator UpdateCameraRotationWithCompass()
    {
        while (Application.isPlaying)
        {
            float trueHeading = Input.compass.trueHeading;
            transform.rotation = Quaternion.identity * Quaternion.Euler(0, trueHeading, 0);

            yield return new WaitForSecondsRealtime(UpdateInterval);
        }
    }
}
