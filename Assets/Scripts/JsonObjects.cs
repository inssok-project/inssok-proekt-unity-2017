using System;
using System.Collections.Generic;

[Serializable]
public class JsonPointOfInterest
{
    public int id;
    public string name;
    public string description;
    public string image;
    public float latitude;
    public float longitude;
    public float width;
    public float length;
    public float height;
}

[Serializable]
public class JsonPointOfInterestList
{
    public List<JsonPointOfInterest> points;
}