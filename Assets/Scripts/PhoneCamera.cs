﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneCamera : MonoBehaviour
{
    private WebCamTexture _webCamTexture;
    private bool _camAvailable;

    public RawImage Background;
    public AspectRatioFitter Fitter;

    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No camera detected.");
            _camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; ++i)
        {
            if (!devices[i].isFrontFacing)
            {
                _webCamTexture = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }

        if (_webCamTexture == null)
        {
            Debug.Log("Unable to find camera.");
            return;
        }

        _webCamTexture.Play();
        Background.texture = _webCamTexture;

        _camAvailable = true;
    }

    void Update()
    {
        if (!_camAvailable)
        {
            return;
        }

        float ratio = (float)_webCamTexture.width / (float)_webCamTexture.height;
        Fitter.aspectRatio = ratio;

        float scaleY = _webCamTexture.videoVerticallyMirrored ? -1f : 1f;
        Background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

		int orient = -_webCamTexture.videoRotationAngle;
		Background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    }
}
