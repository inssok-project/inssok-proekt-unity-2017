using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PointDetailsDialog : MonoBehaviour
{
    public RawImage PointImage;
    public Text PointName;
    public Text PointDescription;
    [Tooltip("The default image texture.")]
    public Texture DefaultTexture;

    private int _activePointId;

    void Awake()
    {
        gameObject.SetActive(false);
    }

    public void Show(PointOfInterest pointOfInterest)
    {
        if (isActiveAndEnabled && _activePointId == pointOfInterest.Id)
        {
            return;
        }

        PointName.text = pointOfInterest.PointName;
        PointDescription.text = pointOfInterest.PointDescription;
        PointImage.texture = DefaultTexture;
        _activePointId = pointOfInterest.Id;

        gameObject.SetActive(true);

        StartCoroutine(LoadImage(pointOfInterest.ImageName));
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator LoadImage(string imageName)
    {
        string url = APIHelper.GetImageByNameQuery(imageName);
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            yield break;
        }

        Texture myTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;

        PointImage.texture = myTexture;
    }
}