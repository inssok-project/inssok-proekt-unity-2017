﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPSHelper
{
    public static Vector3 GPSPointsToWorldPosition(Vector2 gpsCoordinates)
    {
        var worldPosition = Vector3.zero;
        var horizontalOffset = Haversine(new Vector2(0, 0), new Vector2(0, gpsCoordinates.y));
        Debug.LogFormat("Horizontal offset: {0}\n", horizontalOffset);
        var verticalOffset = Haversine(new Vector2(0, 0), new Vector2(gpsCoordinates.x, 0));
        Debug.LogFormat("Vertical offset: {0}\n", verticalOffset);

        worldPosition.x = horizontalOffset;
        worldPosition.z = verticalOffset;

        return worldPosition;
    }

    public static Vector3 GPSPointsToWorldOffset(Vector2 originPoint, Vector2 destinationPoint)
    {
        var worldOffset = Vector3.zero;

        var horizontalOffsetOrigin = Haversine(new Vector2(originPoint.x, originPoint.y), new Vector2(originPoint.x, destinationPoint.y));
        var horizontalOffsetDestination = Haversine(new Vector2(destinationPoint.x, originPoint.y), new Vector2(destinationPoint.x, destinationPoint.y));
        var horizontalOffsetAverage = (horizontalOffsetOrigin + horizontalOffsetDestination) / 2;
        if (originPoint.x > destinationPoint.x)
        {
            horizontalOffsetAverage = -horizontalOffsetAverage;
        }

        var verticalOffsetOrigin = Haversine(new Vector2(originPoint.x, originPoint.y), new Vector2(destinationPoint.x, originPoint.y));
        var verticalOffsetDestination = Haversine(new Vector2(originPoint.x, destinationPoint.y), new Vector2(destinationPoint.x, destinationPoint.y));
        var verticalOffsetAverage = (verticalOffsetOrigin + verticalOffsetDestination) / 2;
        if (originPoint.y > destinationPoint.y)
        {
            verticalOffsetAverage = -verticalOffsetAverage;
        }

        worldOffset.x = horizontalOffsetAverage;
        worldOffset.z = verticalOffsetAverage;

        return worldOffset;
    }

    public static float Haversine(Vector2 originPoint, Vector2 destinationPoint)
    {
        const double r = 6371;

        var lat = (destinationPoint.x - originPoint.x).ToRadians();
        var lng = (destinationPoint.y - originPoint.y).ToRadians();

        var h = Math.Sin(lat / 2) * Math.Sin(lat / 2) +
            Math.Cos(originPoint.x.ToRadians()) * Math.Cos(destinationPoint.x.ToRadians()) *
            Math.Sin(lng / 2) * Math.Sin(lng / 2);
        h = 2 * Math.Asin(Math.Min(1, Math.Sqrt(h)));

        return (float)(r * h);
    }
}
