﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraGPSPosition : MonoBehaviour
{
    [Tooltip("The interval of updating the GPS location of the phone.")]
    public float UpdateInterval = 2f;
    [Tooltip("The minimum distance for refreshing the surrounding points of interest.")]
    public float MinimumRefreshDistance = 10f;
    [Tooltip("The script responsible for managing the instantiated points of interest.")]
    public PointOfInterestManager PointOfInterestManager;
    public Text InfoText;

    [HideInInspector]
    public Vector3 CurrentPosition;

    void Start()
    {
        CurrentPosition = transform.position;

        Input.location.Start();
        StartCoroutine(UpdateCameraLocationWithGPS());
    }

    private IEnumerator UpdateCameraLocationWithGPS()
    {
        if (!Input.location.isEnabledByUser)
        {
            InfoText.text = "GPS is required for this application to work.";
        }

        yield return new WaitUntil(() => Input.location.status == LocationServiceStatus.Running);
        InfoText.text = "";

        while (Application.isPlaying)
        {
            //InfoText.text = string.Format("{0}, {1}", CurrentPosition, transform.rotation.eulerAngles);
            var gpsData = Input.location.lastData;
            var gpsCoordinates = new Vector2(gpsData.latitude, gpsData.longitude);
            var worldPosition = GPSHelper.GPSPointsToWorldPosition(gpsCoordinates);
            transform.position = worldPosition;

            if (Vector3.Distance(CurrentPosition, worldPosition) > MinimumRefreshDistance)
            {
                CurrentPosition = worldPosition;
                PointOfInterestManager.UpdatePoints();
            }

            yield return new WaitForSecondsRealtime(UpdateInterval);
        }
    }
}
